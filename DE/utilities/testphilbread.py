import time

from philbread import Breadboard_control

bread = Breadboard_control(37, 31, 11) # Green LED, Red LED, Button Input

bread.green_toggle()
bread.red_toggle()

bread.green_toggle()
bread.red_toggle()
while True:
    print(bread.is_button())
    time.sleep(0.5)

